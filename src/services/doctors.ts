import BaseService from "./base";
import { ObjectId } from 'mongodb';

export interface WorkingHours {
    from: string;
    to: string;
}

export enum DayOfWeek {
    Monday = 'Monday',
    Tuesday = 'Tuesday',
    Wednesday = 'Wednesday',
    Thursday = 'Thursday',
    Friday = 'Friday',
}

export interface Doctor {
    name: string;
    speciality: string;
    working_hours: {
        [key in DayOfWeek]?: WorkingHours[];
    };
}

export class DoctorsService extends BaseService {
    async getDoctors(): Promise<Doctor[]> {
        try {
            const doctors = await this.model.collection<Doctor>('doctors').find().toArray();
            return doctors;
        } catch (error) {
            console.error('Failed to get doctors', error);
            throw error;
        }
    }

    async getDoctorById(doctorId: string): Promise<Doctor | null> {
        try {
            const doctorData = await this.model.collection('doctors').findOne({ _id: new ObjectId(doctorId) });
            if (!doctorData) {
                return null;
            }
            const { name, speciality, working_hours } = doctorData;
            return { name, speciality, working_hours } as Doctor;
        } catch (error) {
            console.error('Error retrieving doctor:', error);
            return null;
        }
    }
}


