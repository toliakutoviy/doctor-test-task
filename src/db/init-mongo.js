db = db.getSiblingDB('doctor-test-task');

db.users.insertMany([
    {name: "John Doe", phone: "+30965412369", timezone: "+02:00"},
    {name: "Jane Smith", phone: "+30965412369", timezone: "-08:00"},
    {name: "Bob Johnson", phone: "+30965412369", timezone: "+01:00"}
]);

db.doctors.insertMany([
    {
        name: "Doc Petya",
        speciality: "dentist",
        working_hours: {
            Monday: [{from: '09:00', to: '12:00'}, {from: '13:00', to: '15:00'}],
            Tuesday: [{from: '10:00', to: '14:00'}],
            Wednesday: [{from: '08:00', to: '12:00'}],
            Friday: [{from: '08:00', to: '12:00'}],
        }
    },
    {
        name: "Doc Jane", speciality: "pediatrician", working_hours: {
            Monday: [{from: '09:00', to: '12:00'}, {from: '13:00', to: '15:00'}],
            Tuesday: [{from: '10:00', to: '14:00'}],
            Wednesday: [{from: '08:00', to: '12:00'}],
            Thursday: [{from: '08:00', to: '12:00'}],
            Friday: [{from: '08:00', to: '12:00'}],
        }
    },
    {
        name: "Doc Michigan", speciality: "surgeon", working_hours: {
            Thursday: [{from: '08:00', to: '12:00'}],
            Friday: [{from: '08:00', to: '12:00'}],
            Saturday: [{from: '08:00', to: '12:00'}],
        }
    },
]);