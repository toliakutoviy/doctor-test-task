import { Request, Response } from 'express';
import { User, UsersService } from '../services/users';

class UsersController {
    private usersService: UsersService;

    constructor() {
        this.usersService = new UsersService();
    }

    async getAllUsers(req: Request, res: Response): Promise<void> {
        try {
            const users: User[] = await this.usersService.getUsers();
            res.json(users);
        } catch (error) {
            console.error('Failed to fetch users', error);
            throw new Error('Failed to fetch users')
        }
    }
}

export default UsersController;