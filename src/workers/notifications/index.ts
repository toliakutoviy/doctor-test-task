import {ObjectId} from "mongodb";
import { format, utcToZonedTime } from 'date-fns-tz';
import cron from 'node-cron';

import db from "../../db";

const notificationTiming = [{
    delay: 24 * 60 * 60 * 1000,
    key: "dayNotification",
    template: `Hello {{ user.name }}! Remember that you have an appointment to {{ doctor.speciality }} tomorrow at {{ time }}!`
}, {
    delay: 2 * 60 * 60 * 1000,
    key: "twoHoursNotification",
    template: `Hello {{ user.name }}! You have an appointment to {{ doctor.speciality }} in 2 hours at {{ time }}!`
}]

async function checkAppointmentsForNotification() {
    const currentDate = new Date();
    try {
        const appointmentsCollection = db.collection('appointments');
        for (const { delay, key, template } of notificationTiming) {
            const appointments = await appointmentsCollection.aggregate([
                {
                    $match: {
                        [key]: false,
                        time: {$gte: currentDate.getTime(), $lte: currentDate.getTime() + delay}
                    }
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'user_id',
                        foreignField: '_id',
                        as: 'user'
                    }
                },
                {
                    $lookup: {
                        from: 'doctors',
                        localField: 'doctor_id',
                        foreignField: '_id',
                        as: 'doctor'
                    }
                }
            ]).toArray();
            for (const appointment of appointments) {
                const user = appointment.user[0]
                const doctor = appointment.doctor[0]
                const userDateTime = utcToZonedTime(appointment.time, user?.timezone);
                const formattedTime = format(userDateTime, 'HH:mm');
                console.log(template.replace('{{ user.name }}', user?.name).replace('{{ doctor.speciality }}', doctor?.speciality).replace('{{ time }}', formattedTime));
            }
            await db.collection('appointments').updateMany(
                { _id: { $in: appointments.map(appointment => new ObjectId(appointment._id)) } },
                { $set: { [key]: true } }
            );
        }
    } catch (error) {
        console.error('Error retrieving appointments:', error);
    }
}

cron.schedule('*/5 * * * *', checkAppointmentsForNotification);