import BaseService from "./base";

export interface User {
    name: string;
    phone: string;
    timezone: string;
}

export class UsersService extends BaseService {
    async getUsers(): Promise<User[]> {
        try {
            const users = await this.model.collection<User>('users').find().toArray();
            return users;
        } catch (error) {
            console.error('Failed to get users', error);
            throw error;
        }
    }
}

