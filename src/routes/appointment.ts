import express from "express";
import AppointmentsController from '../controllers/appointments';

const router: express.Router = express.Router();

const appointmentsController = new AppointmentsController();
router.route('/').get(appointmentsController.getAllAppointments.bind(appointmentsController));

router.route('/').post(appointmentsController.createAppointment.bind(appointmentsController));
router.route('/:id').delete(appointmentsController.deleteAppointment.bind(appointmentsController));

export default router


