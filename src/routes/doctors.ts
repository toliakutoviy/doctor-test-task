import express from "express";
import DoctorsController from '../controllers/doctors';

const router: express.Router = express.Router();

const doctorsController = new DoctorsController();
router.route('/').get(doctorsController.getAllDoctors.bind(doctorsController));

export default router


