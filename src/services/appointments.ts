import {ObjectId} from "mongodb"
import { parse } from 'date-fns';

import {DoctorsService, Doctor, WorkingHours, DayOfWeek} from "./doctors";

import BaseService from "./base";

export interface Appointment {
    user_id: string;
    doctor_id: string;
    time: Date;
    duration: number;
    dayNotification?: boolean;
    twoHoursNotification?: boolean;
    finishTime?: Date;
}

export class AppointmentsService extends BaseService {
    private doctorService: DoctorsService
    constructor() {
        super();
        this.doctorService = new DoctorsService()
    }

    private async isAppointmentWithinWorkingHours(appointment: Appointment, doctor: Doctor): Promise<boolean> {
        const appointmentTimeDate = new Date(appointment.time)
        const appointmentDayOfWeek: DayOfWeek = appointmentTimeDate.toLocaleString('en-us', {weekday:'long'}) as DayOfWeek;
        const doctorWorkingHours = doctor.working_hours[appointmentDayOfWeek];

        if (!doctorWorkingHours) {
            return false; // Doctor doesn't work that day
        }

        const appointmentTime = appointmentTimeDate.getTime();
        for (const workingHour of doctorWorkingHours) {
            const startTime = parse(workingHour.from, 'HH:mm', new Date(appointment.time)).getTime();
            const endTime = parse(workingHour.to, 'HH:mm', new Date(appointment.time)).getTime();

            if (appointmentTime >= startTime && appointmentTime + appointment.duration * 60 * 1000 <= endTime) {
                return true; // Appointment good
            }
        }

        return false; // Not matched hours
    }

    private async isAppointmentOverlapping(newAppointment: Appointment ): Promise<boolean> {
        const timestamp = (new Date(newAppointment.time)).getTime();
        const ftimestamp = timestamp + newAppointment.duration * 60 * 1000
        const existingAppointments = await this.model.collection('appointments').find({
            doctor_id: new ObjectId(newAppointment.doctor_id),
            $or: [
                // Case 1: Appointment starts with new appointment
                { time: { $gte: timestamp, $lt: ftimestamp } },
                // Case 2: Appointment ends wuth new appointment
                { finishTime: { $gt: timestamp, $lte: ftimestamp } },
                // Case 3: Appointment overlaps with new appointment
                { time: { $lte: timestamp }, finishTime: { $gte: ftimestamp } }
            ]
        }).toArray();
        return existingAppointments.length > 0;
    }

    async getAppointments(): Promise<Appointment[]> {
        try {
            const appointments = await this.model.collection<Appointment>('appointments').find().toArray();
            return appointments;
        } catch (error) {
            throw error;
        }
    }

    async createAppointment(appointment: Appointment): Promise<void> {
        try {
            if (appointment.duration < 20 || appointment.duration > 120) {
                throw new Error('Invalid duration');
            }
            const doctor: Doctor | null = await this.doctorService.getDoctorById(appointment.doctor_id)
            if (!doctor) {
                throw new Error('Doctor not found.');
            }

            const isWithinWorkingHours: boolean = await this.isAppointmentWithinWorkingHours(appointment, doctor);

            if (!isWithinWorkingHours) {
                throw new Error('Appointment is outside of doctors working hours.');
            }

            const overlapExists: boolean = await this.isAppointmentOverlapping(appointment);
            if(overlapExists) {
                throw new Error('There is overlap with existing appointment.');
            }

            appointment.dayNotification = false
            appointment.twoHoursNotification = false

            await this.model.collection('appointments').insertOne({
                ...appointment,
                time: (new Date(appointment.time)).getTime(),
                finishTime: (new Date(appointment.time)).getTime() + appointment.duration * 60000,
                user_id: new ObjectId(appointment.user_id),
                doctor_id: new ObjectId(appointment.doctor_id)
            });

        } catch (error) {
            throw error;
        }
    }

    async deleteAppointment(appointmentId: string): Promise<any> {
        const result = await this.model.collection('appointments')
            .deleteOne({ _id: new ObjectId(appointmentId) });

        return result;
    }
}

