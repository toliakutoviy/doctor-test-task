import {Db} from "mongodb";
import db from '../db'

class BaseService {
    protected model: Db;

    constructor() {
        this.model = db;
    }
}

export default BaseService;