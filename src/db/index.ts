import { MongoClient, MongoClientOptions } from 'mongodb';

const url = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}`;

const dbName = process.env.MONGO_DB;

const options = {
    useUnifiedTopology: true,
    useNewUrlParser: true,
} as MongoClientOptions;

const client = new MongoClient(url, options);

async function connect() {
    try {
        await client.connect();
        console.log('Connected to MongoDB');
    } catch (err) {
        console.error('Failed to connect to MongoDB', err);
    }
}

connect();

export default client.db(dbName);