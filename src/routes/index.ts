import express from 'express';
import doctorsRoutes from "./doctors";
import usersRoutes from "./users";
import appointmentsRoutes from "./appointment";

const router: express.Router = express.Router();

router.use('/doctors', doctorsRoutes);
router.use('/users', usersRoutes);
router.use('/appointments', appointmentsRoutes);

export default router;