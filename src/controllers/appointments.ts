import { Request, Response, NextFunction } from 'express';

import { Appointment, AppointmentsService } from '../services/appointments';

class AppointmentsController {
    private appointmentsService: AppointmentsService;

    constructor() {
        this.appointmentsService = new AppointmentsService();
    }

    async getAllAppointments(req: Request, res: Response): Promise<void> {
        try {
            const appointments: Appointment[] = await this.appointmentsService.getAppointments();
            res.json(appointments);
        } catch (error) {
            console.error('Failed to fetch appointments', error);
            throw new Error('Failed to fetch appointments')
        }
    }

    async createAppointment(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            await this.appointmentsService.createAppointment(req.body);
            res.status(201).json({ message: 'Appointment created successfully' })
        } catch (error) {
            next(error)
        }
    }

    async deleteAppointment(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            await this.appointmentsService.deleteAppointment(req.params.id);
            res.status(201).json({ message: 'Appointment deleted successfully' })
        } catch (error) {
            next(error)
        }
    }
}

export default AppointmentsController;