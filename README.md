# Doctor Test Task
### requirements:
docker
docker-compose

### start

```
docker-compose up
```

### config file - .env

### DB 
I used plain mongo, no orm. didnt see particular reason to use one for few endpoints

/src/db/init-mongo.js - file with mock data.

### notifications
In real application there should be some message broker with scheduling messages. Kafka, RabbitMQ, etc. But implement it in this case, too much, used simple cron which runs every 5 minutes

Cron located in src/workers folder.

### main endpoints

 - GET http://localhost:5000/users
```
[
    {
    "_id": "649d3bbcd382352a207036c0",
    "name": "John Doe",
    "phone": "+30965412369",
    "timezone": "+02:00"
    },
    ....
```
 - GET http://localhost:5000/doctors
```
[
    {
        "_id": "649d3bbcd382352a207036c3",
        "name": "Doc Petya",
        "speciality": "dentist",
        "working_hours": {
            "Monday": [
                {
                    "from": "09:00",
                    "to": "12:00"
                },
            ]
        }
    },
    ....
```
 - GET http://localhost:5000/appointments
```
[
    {
        "_id": "649d3d8d0e618b2efb284a9e",
        "user_id": "649d3bbcd382352a207036c0",
        "doctor_id": "649d3bbcd382352a207036c5",
        "time": 1688112900000,
        "duration": 20,
        "dayNotification": false,
        "twoHoursNotification": false,
        "finishTime": 1688114100000
    }
]
``` 
 - POST http://localhost:5000/appointments
Body: 
```
{
    "user_id": "649d3bbcd382352a207036c0",
    "doctor_id": "649d3bbcd382352a207036c5",
    "time": "2023-06-30T08:15:00.000Z",
    "duration": 20
}
```
PAY ATTENTION:
doctor schedule in UTC timezone, but appointment receives date with any timezone 2023-06-30T08:15:00.000Z or 2023-06-30T08:15:00.00+0200 both work
 - DELETE http://localhost:5000/appointments/:id

