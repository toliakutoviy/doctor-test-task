import express from "express";
import UsersController from '../controllers/users';

const router: express.Router = express.Router();

const usersController = new UsersController();
router.route('/').get(usersController.getAllUsers.bind(usersController));

export default router


