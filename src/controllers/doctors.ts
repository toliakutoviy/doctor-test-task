import { Request, Response } from 'express';
import { Doctor, DoctorsService } from '../services/doctors';

class DoctorsController {
    private doctorsService: DoctorsService;

    constructor() {
        this.doctorsService = new DoctorsService();
    }

    async getAllDoctors(req: Request, res: Response): Promise<void> {
        try {
            const doctors: Doctor[] = await this.doctorsService.getDoctors();
            res.json(doctors);
        } catch (error) {
            console.error('Failed to fetch doctors', error);
            throw new Error('Failed to fetch doctors')
        }
    }
}

export default DoctorsController;